==========
ADP-utils
==========

A few utilities to ease the input creation for the `Automatic DFTB
Parametrization Toolkit <https://bitbucket.org/solccp/adpt_core>`_. Their aim is
to enable ADPTs usage without having to click or type too much. This is achieved
by automatizing the generation of various input files using a lean input file
and the outputs of existing calculations.

The project is `hosted on bitbucket <http://bitbucket.org/aradi/adputils>`_ and
released under the *BSD 2-clause license*.


Dependencies
============

In order to use the utilities, you need following Python-requirements:

* Python 3.2 or later

* yaml package for Python 3 (e.g. python3-yaml on Ubuntu/Debian)

* `Atomic Simulation Environment <https://wiki.fysik.dtu.dk/ase/>`_
